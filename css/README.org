#+title: Site CSS
#+author: Martin Baulig
#+PROPERTY: header-args :mkdirp yes :comments link


This is a literate configuration file written in /Emacs Org Mode/, containing all the style information.

* Base style

Mostly copied from [[https://raw.github.com/jaspervdj/hakyll/master/web/css/syntax.css]].

#+begin_src scss :tangle syntax/syntax-highlighting-base.scss
code {
    border: none;
}

pre {
    code {
        background-color: inherit;
        border: inherit;
    }
}

table.sourceCode,
tr.sourceCode,
td.lineNumbers,
td.sourceCode,
table.sourceCode pre {
    margin: 0;
    padding: 0;
    border: 0;
    vertical-align: baseline;
    border: none;
}

td.lineNumbers {
    border-right: 1px solid #aaaaaa;
    text-align: right;
    color: #aaaaaa;
    padding-right: 5px;
    padding-left: 5px;
}

td.sourceCode {
    padding-left: 5px;
}
#+end_src

* Color Theme

I am not a huge fan an the ~Solarized Light~ theme - and actually use ~Monokai Pro~ in my Doom Emacs.  However, I couldn't find any good coloring for it online, so I'm just sticking to it for the moment.

#+begin_src scss :tangle syntax/solarized-light.scss
// Solarized Light color palette.
$base03: #002b36;
$base02: #073642;
$base01: #586e75;
$base00: #657b83;
$base0: #839496;
$base1: #93a1a1;
$base2: #eee8d5;
$base3: #fdf6e3;
$yellow: #b58900;
$orange: #cb4b16;
$red: #dc322f;
$magenta: #d33682;
$violet: #6c71c4;
$blue: #268bd2;
$cyan: #2aa198;
$green: #859900;
#+end_src

The default colors for source code blocks don't look very well when using the ~Dark Reader~ extension in ~Firefox~, so I'm overriding them here.  This gray looks great using both a light and dark reading mode.

#+begin_src scss :tangle syntax/solarized-light.scss
$code_color: #fcfcfa;
$code_background: #2d2a2e;
#+end_src

Let's pick some colors for the navigation bar at the bottom.

#+begin_src scss :tangle syntax/solarized-light.scss
$nav_color: $base3;
$nav_text: $base03;
$nav_background: $base1;
#+end_src

Activate those colors in source code blocks.

#+begin_src scss :tangle syntax/solarized-light.scss
pre,
code {
    color: $code_color;
    background-color: $code_background;
}
#+end_src

The rest of the file should be the same for all color themes, so we put those into a separate file and =@include= the actual color theme that we just defined above.

#+begin_src scss :tangle syntax/syntax-highlighting.scss
@import "syntax-highlighting-base";
@import "solarized-light";
#+end_src

Here goes the rest of the file.

#+begin_src scss :tangle syntax/syntax-highlighting.scss
.sourceCode {
    // KeyWordTok
    .kw {
        color: $blue;
    }
    // DataTypeTok
    .dt {
        color: $blue;
    }
    // DecValTok (decimal value), BaseNTok, FloatTok
    .dv,
    .bn,
    .fl {
        color: $magenta;
    }
    // CharTok
    .ch {
        color: $red;
    }
    // StringTok
    .st {
        color: $cyan;
    }
    // CommentTok
    .co {
        color: $base1;
    }
    // OtherTok
    .ot {
        color: $yellow;
    }
    // AlertTok
    .al {
        color: $orange;
        font-weight: bold;
    }
    // FunctionTok
    .fu {
        color: $blue;
    }
    // RegionMarkerTok
    .re {
    }
    // ErrorTok
    .er {
        color: $red;
        font-weight: bold;
    }
}
#+end_src

* Main Style File

This is the main style file that includes everything else.

First, we need to include the color theme for syntax highlighting.

#+begin_src scss :tangle default.scss
@import "syntax/syntax-highlighting";
#+end_src

Add some padding around the source code blocks.

#+begin_src scss :tangle default.scss
pre {
    padding: 5px;
}

details {
    margin: 5px 0px 25px 0px;
}

pre.sourceCode {
    padding: 10px;
}

.div #outline-2 {
    margin-bottom: 30px;
}
#+end_src

Make sure we resize the header image to fit the entire screen width.

#+begin_src scss :tangle default.scss
img.header-image {
    object-fit: cover;
    width: 100%;
}
#+end_src

* Bottom navigation bar

This is the entire footer; we place it at the bottom at make it stretch the entire width.

#+begin_src scss :tangle default.scss
footer {
    overflow: hidden;
    position: fixed;
    bottom: 0;
    width: 100%;
}
#+end_src

The panel inside of it.

#+begin_src scss :tangle default.scss
.footer-panel {
    color: $nav_color;
    background-color: $nav_background;
    
    display: grid;
    grid-template-columns: auto auto;

    justify-content: space-between;

    padding: 10px;

    div {
        padding: 5px 20px 5px 20px;
    }

    .navbar {
        grid-row: 1;
        grid-column: 1;
    }

    .hakyll {
        grid-row: 1;
        grid-column: 2;
    }

    .generated {
        grid-row: 2;
        grid-column: 1 / 2;
    }

    span {
        color: $nav_text;
        text-align: center;
        text-decoration: none;
        font-size: 17px;
    }
}
#+end_src

Misc cosmetic items.

#+begin_src scss :tangle default.scss
/* Change the color of links on hover */
.navbar a:hover {
    background-color: #ddd;
    color: black;
}

/* Add a color to the active/current link */
.navbar a.active {
    background-color: #04aa6d;
    color: white;
}
#+end_src
