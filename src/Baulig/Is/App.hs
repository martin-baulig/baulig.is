module Baulig.Is.App
    ( App
    , HasApp(..)
    , runApp
    ) where

import           Baulig.Prelude

import           RIO.Directory
import           RIO.Time

import           Baulig.Is.AppSettings
import           Baulig.Is.GitHelper
import           Baulig.Utils.Bottleneck
import           Baulig.Utils.GitCommit
import           Baulig.Utils.LocalPath

------------------------------------------------------------------------------
--- AppSetup
------------------------------------------------------------------------------
data App = App { _appSettings    :: !AppSettings
               , _appLogFunc     :: !LogFunc
               , _appRootPath    :: !AbsoluteLocalPath
               , _appCurrentTime :: !ZonedTime
               , _appRepoHead    :: !GitCommit
               }

instance Display App where
    display this = display $ this <:> _appSettings this <+> _appRootPath this
        <+> _appCurrentTime this <+> _appRepoHead this

instance HasLogFunc App where
    logFuncL = lens _appLogFunc (\x y -> x { _appLogFunc = y })

------------------------------------------------------------------------------
--- HasApp
------------------------------------------------------------------------------
class (HasAppSettings env, HasLogFunc env) => HasApp env where
    app :: Lens' env App

    appDebug :: Lens' env Bool
    appDebug = app . appSettings . appSettingsDebug

    appRootPath :: SimpleGetter env AbsoluteLocalPath
    appRootPath = app . to _appRootPath

    appCurrentTime :: SimpleGetter env ZonedTime
    appCurrentTime = app . to _appCurrentTime

    appRepoHead :: SimpleGetter env GitCommit
    appRepoHead = app . to _appRepoHead

instance HasApp App where
    app = id

instance HasAppSettings App where
    appSettings = lens _appSettings (\x y -> x { _appSettings = y })

------------------------------------------------------------------------------
--- App
------------------------------------------------------------------------------
defaultLogOptions :: AppSettings -> IO LogOptions
defaultLogOptions settings = do
    logOptions <- logOptionsHandle stderr True
    pure $ setLogMinLevel (view appSettingsLogLevel settings)
        $ setLogUseTime False $ setLogUseLoc False
        $ setLogSecondaryColor "\ESC[38;5;245m" logOptions

runApp :: RIO App () -> IO ()
runApp inner = do
    settings <- parseCommandLineArguments
    logOptions <- defaultLogOptions settings
    cwd <- getCurrentDirectory

    rootPath <- absoluteRootPath <$> canonicalizePath cwd

    zonedTime <- getZonedTime
    head <- getRepositoryHead rootPath

    withBottleneckLogger logOptions $ \logFunc -> do
        flip runRIO inner $ App settings logFunc rootPath zonedTime head
