module Baulig.Is.Context
    ( indexTemplate
    , literateContext
    , literateTemplate
    , postContext
    ) where

import           RIO

import           Hakyll

---------------------------------------------------------------------------
literateContext :: Context String
literateContext = defaultContext

postContext :: Context String
postContext = dateField "date" "%B %e, %Y" `mappend` defaultContext

literateTemplate :: Identifier
literateTemplate = "templates/default.html"

indexTemplate :: Identifier
indexTemplate = "templates/index.html"
