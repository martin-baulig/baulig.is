
module Baulig.Is.GitHelper
    ( CommitHash
    , getRepositoryHead
    ) where

import           Baulig.Prelude

import           Git
import qualified Git.Libgit2            as LG

import           Baulig.Is.GitException
import           Baulig.Utils.GitCommit
import           Baulig.Utils.LocalPath

repositoryOptions :: AbsoluteLocalPath -> RepositoryOptions
repositoryOptions (absoluteFilePath -> root) =
    RepositoryOptions { repoPath       = root
                      , repoWorkingDir = Just root
                      , repoIsBare     = False
                      , repoAutoCreate = False
                      }

getRepositoryHead :: MonadIO m => AbsoluteLocalPath -> m GitCommit
getRepositoryHead root =
    liftIO $ withRepository' LG.lgFactory (repositoryOptions root) $ do
        commit <- resolveCommit' "HEAD"
        case commit of
            Nothing ->
                throwIO $ GitException Nothing "Failed to resolve HEAD commit."
            Just commit' -> pure $ fromGitLibCommit commit'

resolveCommit' :: (LG.HasLgRepo m, LG.MonadLg m)
               => RefName
               -> m (Maybe (Commit LG.LgRepo))
resolveCommit' name = do
    ref <- resolveReference name
    case ref of
        Nothing -> pure Nothing
        Just ref' -> do
            obj <- lookupObject ref'
            pure $ case obj of
                CommitObj commitObj -> Just commitObj
                _ -> Nothing

