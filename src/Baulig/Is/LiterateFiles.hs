module Baulig.Is.LiterateFiles (literateFiles) where

import           RIO

import           Hakyll

import           Baulig.Is.Context
import           Baulig.Is.PandocOptions

---------------------------------------------------------------------------
-- | Compute Hakyll routes for all literate source files in this project.
literateFiles :: Rules ()
literateFiles = do
    -- The top-level README.org file is special.
    -- We override the title and hard-code the path to "index.html".
    match "README.org" $ do
        route $ constRoute "index.html"
        compile $ pandocCompilerWithOptions
            >>= loadAndApplyTemplate literateTemplate
                                     (constField "title" "Martin Baulig"
                                      `mappend` literateContext)
            >>= relativizeUrls

    -- For the rest of the site, we prefix "code/".
    match literateFileList $ do
        route $ gsubRoute "" ("code/" ++)
            `composeRoutes` setExtension "html"
        compileLiteral

    -- Create a list of all literate source files.
    create ["code.html"] $ do
        route idRoute
        compile $ do
            codeFiles <- loadAll literateFileList
            let codeCtx = constField "title" "Literate Source Code"
                    `mappend` literateContext
            let archiveCtx =
                    listField "code" literateContext (return codeFiles)
                    `mappend` codeCtx

            makeItem ""
                >>= loadAndApplyTemplate "templates/code.html" archiveCtx
                >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                >>= relativizeUrls


literateFileList :: Pattern
literateFileList = "css/*.org"


---------------------------------------------------------------------------
--- Utilities
---------------------------------------------------------------------------
compileLiteral :: Rules ()
compileLiteral = compile $ pandocCompilerWithOptions
    >>= loadAndApplyTemplate literateTemplate literateContext >>= relativizeUrls
