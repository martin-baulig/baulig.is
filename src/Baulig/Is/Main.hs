module Baulig.Is.Main (main) where

import           Baulig.Prelude

import           RIO.Time

import           Baulig.Is.App
import           Baulig.Is.GitHelper
import           Baulig.Is.Site

---------------------------------------------------------------------------
main :: IO ()
main = runApp $ do
    appRoot <- view appRootPath
    zone <- getCurrentTimeZone
    utcTime <- getCurrentTime
    zonedTime <- getZonedTime
    head <- getRepositoryHead appRoot
    logDebug $ display $ "Root directory" <:=> appRoot <+> zone <+> utcTime <+> zonedTime <+> head
    liftIO generateSite
