module Baulig.Is.PandocOptions
    ( pandocCompilerWithOptions
    , pandocReaderOptions
    , pandocWriterOptions
    ) where

import           RIO

import           Hakyll

import           Text.Pandoc.Highlighting
import           Text.Pandoc.Options

---------------------------------------------------------------------------
pandocReaderOptions :: ReaderOptions
pandocReaderOptions = defaultHakyllReaderOptions

pandocWriterOptions :: WriterOptions
pandocWriterOptions =
    defaultHakyllWriterOptions { writerHighlightStyle = Just haddock }

pandocCompilerWithOptions :: Compiler (Item String)
pandocCompilerWithOptions =
    pandocCompilerWith pandocReaderOptions pandocWriterOptions
