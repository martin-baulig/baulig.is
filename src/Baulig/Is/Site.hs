module Baulig.Is.Site (generateSite) where

import           RIO

import           Hakyll
import           Hakyll.Web.Sass

import           Baulig.Is.LiterateFiles
import           Baulig.Is.PandocOptions

---------------------------------------------------------------------------
generateSite :: IO ()
generateSite = hakyll $ do
    -- Other Assets
    match "assets/**" $ do
        route idRoute
        compile copyFileCompiler

    -- Styles (SASS)
    match "css/*.scss" $ do
        route $ setExtension "css"
            `composeRoutes` gsubRoute "sass/" (const "css/")
        compile (fmap compressCss <$> sassCompiler)

    literateFiles

    match "css/README.org" $ do
        route $ gsubRoute "css/" ("code/" ++)
            `composeRoutes` setExtension "html"
        compile $ pandocCompilerWith pandocReaderOptions pandocWriterOptions
            >>= loadAndApplyTemplate "templates/default.html" postCtx
            >>= relativizeUrls

    match (postPattern "posts") $ do
        route $ setExtension "html"
        compile $ pandocCompilerWith pandocReaderOptions pandocWriterOptions
            >>= loadAndApplyTemplate "templates/post.html" postCtx
            >>= loadAndApplyTemplate "templates/default.html" postCtx
            >>= relativizeUrls

    create ["archive.html"] $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let archiveCtx = listField "posts" postCtx (return posts)
                    `mappend` constField "title" "Archives"
                    `mappend` defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
                >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                >>= relativizeUrls

    match "index.html" $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let indexCtx = listField "posts" postCtx (return posts)
                    `mappend` defaultContext

            getResourceBody >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls

    match "templates/*" $ compile templateBodyCompiler

---------------------------------------------------------------------------
postCtx :: Context String
postCtx = dateField "date" "%B %e, %Y" `mappend` defaultContext

postPattern :: String -> Pattern
postPattern folder = fromGlob (folder ++ "/*.md")
    .||. fromGlob (folder ++ "/*.org")
